package scoreexample;

public class ResultIdentifier {

	void printResult(Score s) {
		System.out.println("The scores are " + s);
		int avg = (s.subject1 + s.subject2 + s.subject3) / 3;
		System.out.println("The average of 3 subjects is " + avg);
		System.out.println(avg > 50 ? "pass" : "fail");
	}

	public static void main(String[] args) {
		ResultIdentifier r = new ResultIdentifier();
		Score s = new Score(45, 65, 70);
		r.printResult(s);
	}
}
