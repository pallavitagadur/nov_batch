package atmexample;

public class Money {
	int value;

	Money(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Money [value=" + value + "]";
	}

}
