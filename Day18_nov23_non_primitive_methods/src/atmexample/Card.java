package atmexample;

public class Card {
	long cardNo;
	int cvv;
	String typeOfCard;
	String cardHolderName;

	Card(long cardNo, int cvv, String typeOfCard, String cardHolderName) {
		this.cardNo = cardNo;
		this.cvv = cvv;
		this.typeOfCard = typeOfCard;
		this.cardHolderName = cardHolderName;
	}

	// ALT + SHIFT + S + S -> to override the toString()
	@Override
	public String toString() {
		return "Card [cardNo=" + cardNo + ", cvv=" + cvv + ", typeOfCard=" + typeOfCard + ", cardHolderName="
				+ cardHolderName + "]";
	}
	
	public static void main(String[] args) {
		Card card = new Card(453632727262626L, 345, "Mastercard", "Alpha");
		System.out.println(card);
	}

}
