package atmexample;

public class ATM {

	// think this is ATM, atm can call the card with different name
	Money withdraw(Card card, int pin) {
		System.out.println("ATM accepted card with details " + card);
		Money m = new Money(100);
		return m;
	}

	// you are the main method, I can call the card with a different name
	public static void main(String[] args) {
		ATM a = new ATM();
		Card c = new Card(453632727262626L, 345, "Mastercard", "Alpha");
		Money paisa = a.withdraw(c, 3423);
		System.out.println(paisa);
	}
}
