
public class ReverseStringBuilderExample {
	public static void main(String[] args) {
		String message = "Hello World";
		StringBuilder sb = new StringBuilder(message);
		sb.reverse();
		String reverseString = sb.toString();
		System.out.println(reverseString);

		System.out.println(new StringBuilder(message).reverse().toString()); // dlroW olleH
	}
}
