
public class SampleSwitchExample2 {
	public static void main(String[] args) {
		char input = 'G';
		switch (input) {
		case 'y', 'Y':
			System.out.println("Yes");
			break;

		case 'n', 'N':
			System.out.println("No");
			break;

		default:
			System.out.println("Not a valid character");
		}
	}
}
// shortcut for arranging code is ctrl + shift + f