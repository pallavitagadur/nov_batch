
public class Car {
	String color;
	String brand;
	boolean isAutomatic;

	// this is just to demonstrate how the compiler
	// rewrites the program and adds default constructor
	Car() {
		this.color = null;
		this.brand = "BWM";
		this.isAutomatic = false;
	}

	public static void main(String[] args) {
		Car c = new Car();
		System.out.println(c.brand); // "BMW"
		System.out.println(c.color); // null
		System.out.println(c.isAutomatic); // false
	}
}
