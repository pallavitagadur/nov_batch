
public class ExampleIfElse {
	public static void main(String[] args) {
		int age = 18;
		char gender = 'F';
		if (age >= 18 && gender == 'F') {
			System.out.println("Eligible for scholarship");
		} else {
			System.out.println("Not eligible");
		}
	}
}
