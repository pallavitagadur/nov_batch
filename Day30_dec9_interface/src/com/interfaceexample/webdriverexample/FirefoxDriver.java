package com.interfaceexample.webdriverexample;

public class FirefoxDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("Opening " + url + " in firefox browser");
	}

	@Override
	public void openPrivate() {
		System.out.println("Opening private window");
	}

}
