
public class WaterBottle {
	// instance(Object) variables
	String color; // default value is null
	int sizeInLitres = 1;
	String brand = "Milton";

	public static void main(String[] args) {
		WaterBottle w1 = new WaterBottle();
		System.out.println(w1.color); // null
		System.out.println(w1.sizeInLitres); // 1
		System.out.println(w1.brand); // Milton
		w1.color = "Green";
		System.out.println(w1.color); // Green

		System.out.println(w1); // we get hexademcial address of the object
	}
}
