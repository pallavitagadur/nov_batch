
public class SampleBlock {

	{
		System.out.println("Instance block");
	}

	{
		System.out.println("instance block 2");
	}

	static {
		System.out.println("Static block");
	}

	static {
		System.out.println("Static block 2");
	}

	public static void main(String[] args) {
		System.out.println("Main method");
		SampleBlock sb1 = new SampleBlock();
		SampleBlock sb2 = new SampleBlock();
	}
}
