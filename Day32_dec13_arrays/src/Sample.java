
public class Sample {
	public static void main(String[] args) {
		int[] marks = { 35, 48, 42 };
		for (int mark : marks) {
			System.out.println(mark);
		}

		System.out.println("_________________");
		String[] names = { "Alpha", "beta", "Charlie" };
		for (String name : names) {
			System.out.print(name + " ");
		}

	}
}
