import java.util.Arrays;

public class SampleColors {
	public static void main(String[] args) {
		String[] colors = { "Red", "Green", "Blue", "Yellow", "Black" };

		System.out.println(Arrays.toString(colors));
	}
}
