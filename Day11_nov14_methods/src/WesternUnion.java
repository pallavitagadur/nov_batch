import java.util.Scanner;

public class WesternUnion {
	static void convertDollarToInr(int dollar, double rate) {
		System.out.println(dollar * rate);
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the dollar value");
		int dollar = s.nextInt();
		System.out.println("Enter today's rate");
		double rate = s.nextDouble();
		WesternUnion.convertDollarToInr(dollar, rate);
		s.close();
	}
}
