
public class SampleArithmeticOperation {
	public static void main(String[] args) {
		System.out.println(4 + 98); // 102
		System.out.println(5 - 3); // 2
		System.out.println(5 * 5); // 25
		System.out.println(5 / 10); // 0
		System.out.println(2 % 25); // 2
	}
}
