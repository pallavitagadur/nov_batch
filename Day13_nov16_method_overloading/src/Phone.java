
public class Phone {
	void unlock(int pin) {
		System.out.println("Unlocked using pin " + pin);
	}

	void unlock(String password) {
		System.out.println("unlocked using password " + password);
	}

	public static void main(String[] args) {
		Phone p = new Phone();
		p.unlock(1234);
		p.unlock("Alpha@123");
	}
}
