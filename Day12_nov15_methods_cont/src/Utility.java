import java.util.Scanner;

public class Utility {

	// instance method
	void greet(String name) {
		System.out.println("Hello " + name);
	}

	// method to count the no. of characters
	static int countCharacters(String word) {
		return word.length();
	}
	
	

	public static void main(String[] args) {
		// creating object of the utility class
		Utility u = new Utility();
		u.greet("Mahesh"); // Hello Mahesh

		// creating a scanner object
		Scanner s = new Scanner(System.in);

		// accepting multi word input using nextLine() of scanner class and storing in
		// variable sentence
		System.out.println("Enter a sentence");
		String sentence = s.nextLine();

		// calling the static method countCharacters and passing String as input and
		// accept int as return from method and storing in the length variable
		int length = Utility.countCharacters(sentence);

		System.out.println("The length of the given word is " + length);
	}
}
