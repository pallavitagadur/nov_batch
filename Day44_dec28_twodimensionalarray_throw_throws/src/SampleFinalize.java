
public class SampleFinalize {

	@Override
	protected void finalize() throws Throwable {
		System.out.println("Calling finalize method");
	}

	public static void main(String[] args) {
		SampleFinalize s = new SampleFinalize();
		s = null;
		
		System.gc();
	}
}
