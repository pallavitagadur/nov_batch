import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class SampleThrows {
	void methodc() throws FileNotFoundException 
	{
		File f = new File("C://hello.txt");
		FileInputStream fis = new FileInputStream(f);
	}

	void methodb() throws FileNotFoundException {
		System.out.println("Start of method b");
		methodc();
		System.out.println("End of method b");
	}

	void methoda() throws FileNotFoundException {
		System.out.println("Start of method a");
		methodb();
		System.out.println("End of method a");
	}

	public static void main(String[] args) throws FileNotFoundException {
		System.out.println("Main start");
		SampleThrows s = new SampleThrows();
		s.methoda();
		System.out.println("main end");
	}
}
